package Tips;

import org.testng.annotations.Test;
import java.io.File;
import java.util.concurrent.TimeUnit;
import org.testng.Reporter;
import org.testng.annotations.*;
import org.apache.commons.io.FileUtils;
import org.openqa.selenium.By;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.ie.InternetExplorerDriver;
import org.openqa.selenium.TakesScreenshot;

import jxl.Sheet;
import jxl.Workbook;



@Test
public class OrderHistory {
	WebDriver driver;
	Workbook wb;
	Sheet sheet;
	
	
  @Test
  @Parameters("browser")
  public void binvokebrowser(String browser) throws Exception {
	  if(browser.equalsIgnoreCase("firefox")){
			//create firefox instance
				System.setProperty("webdriver.gecko.driver", "C:\\Sid_Folder\\Selenium\\geckodriver-v0.23.0-win64\\geckodriver.exe");
				//System.setProperty("webdriver.firefox.marionette", "C:\\Sid_Folder\\Selenium\\geckodriver-v0.23.0-win32\\geckodriver.exe");
				driver = new FirefoxDriver();
				
			}
			//Check if parameter passed as 'chrome'
			else if(browser.equalsIgnoreCase("chrome")){
				//set path to chromedriver.exe
				System.setProperty("webdriver.chrome.driver","C:\\Sid_Folder\\Selenium\\Chromedriver\\chromedriver_win32\\chromedriver.exe");
				//create chrome instance
				driver = new ChromeDriver();
			}
			
			//Check if parameter passed as 'IE'
			else if(browser.equalsIgnoreCase("ie")){
				
			    
		    	System.setProperty("webdriver.ie.driver", "C:\\Sid_Folder\\Selenium\\IEDriverServer_Win32_3.14.0\\IEDriverServer.exe");
		    	driver = new InternetExplorerDriver();
		    	
		    	
			}
			
			else{
				//If no browser passed throw exception
				throw new Exception("Browser is not correct");
			}
	    //System.setProperty("webdriver.chrome.driver", "C:\\Sid_Folder\\Selenium\\Chromedriver\\chromedriver_win32\\chromedriver.exe");
		//driver=new ChromeDriver();
		driver.manage().deleteAllCookies();
		driver.manage().window().maximize();
		driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
		driver.manage().timeouts().pageLoadTimeout(30,TimeUnit.SECONDS);
		


     	//Launch Browser	
    		driver.get("http://ttipsweb03q.tbccorp.com/ntwtips/");
    		Thread.sleep(5000);
    		Reporter.log("TIPS website is launced!");
    			
  }
  
  @Test
  @Parameters("browser")
  public void corderhistory(String browser) throws Exception, Exception {
	     File src= new File("C:/data.xls");
	     System.out.println("Excel located");	     
	     Workbook wb=Workbook.getWorkbook(src);
	     Sheet sheet=wb.getSheet(0);
	     System.out.println("Workbook loaded");

	/*     String data00=wb.getSheet(0).getCell(0, 0).getContents();
	     System.out.println("Data is " +data00);
	     
	     String data01=wb.getSheet(0).getCell(1, 0).getContents();
	     System.out.println("Data is " +data01);*/
	     
	     int rowCount=sheet.getRows();
	     
	     for(int i=0; i<rowCount; i++)
	     {   		

	    	    String username= sheet.getCell(0, i).getContents();
	     		String password=sheet.getCell(1, i).getContents();
	     		String orderno=sheet.getCell(13, i).getContents();
	     		String date=sheet.getCell(14,i).getContents();

	     		
	     	//Login
				driver.findElement(By.id("fldAccount")).sendKeys(username);
				driver.findElement(By.id("fldPassword")).sendKeys(password);
				driver.findElement(By.xpath("//button[@type='submit']")).click();
	            System.out.println("username is" +username);
	            System.out.println("Password is" +password);
	            Thread.sleep(3000);
	            Reporter.log("Login is successful with username" + username + "entered");
				driver.findElement(By.xpath("//ul[@id='ntw-top-menu']/li[8]/a/span")).click();
				Thread.sleep(1000);
			//Order History by date
				driver.findElement(By.id("fldDates")).sendKeys(date);
				driver.findElement(By.name("Search")).click();
				Thread.sleep(3000);
				Reporter.log("Order Search successful with date" +date+ "entered");
	         //Order History by Order no	   
				driver.findElement(By.id("searchBy")).sendKeys(orderno);
				driver.findElement(By.name("Search")).click();
				Thread.sleep(3000);
			// Take screenshot and store as a file format
	            File OrderSrch= ((TakesScreenshot)driver).getScreenshotAs(OutputType.FILE);
	         // now copy the  screenshot to desired location using copyFile //method
	            FileUtils.copyFile(OrderSrch, new File("C:/TIPS/OrderSearch" + i+ browser + ".png"));
				Reporter.log("Order Search successful with " +orderno+ "entered");

			//Items in Order
				driver.findElement(By.xpath("//button[contains(.,'Items')]")).click();
				Thread.sleep(2000);
			// Take screenshot and store as a file format
	            File OrderItems= ((TakesScreenshot)driver).getScreenshotAs(OutputType.FILE);
	         // now copy the  screenshot to desired location using copyFile //method
	            FileUtils.copyFile(OrderItems, new File("C:/TIPS/OrderItems" + i+ browser + ".png"));	
	            Thread.sleep(2000);
	            driver.findElement(By.cssSelector("#dlgOrderLines .close")).click();
	            Thread.sleep(3000);
	            driver.findElement(By.xpath("//ul[@id='ntw-top-menu']/li[4]/a/span")).click();
	            //driver.findElement(By.xpath("//ul[@id='ntw-top-menu']/li[3]/a/span")).click();
				Thread.sleep(5000);
	            
			//Logout
	            
				/*driver.findElement(By.xpath("//ul[@id='ntw-top-menu']/li/a/i")).click();
				Thread.sleep(4000);
				driver.findElement(By.xpath("//ul[@id='ntw-side-menu']/li[14]/a")).click();
				Thread.sleep(4000);
				Reporter.log("Logout successful");*/
				//wb.close();            
	          //}            
  }
}
        @Test
  		public void dclosebrowser() {
  		
         //Browser close and Workbook close	
  			//driver.close();
  			driver.quit();
  			
  		}
}





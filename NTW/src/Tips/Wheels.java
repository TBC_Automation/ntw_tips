package Tips;

import org.testng.annotations.Test;
import java.io.File;

import java.util.concurrent.TimeUnit;

import org.testng.Reporter;
import org.testng.annotations.*;
import org.apache.commons.io.FileUtils;
import org.openqa.selenium.By;

import org.openqa.selenium.Keys;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.WebDriver;

import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.ie.InternetExplorerDriver;

import org.openqa.selenium.TakesScreenshot;

import jxl.Sheet;
import jxl.Workbook;



@Test
public class Wheels {
	WebDriver driver;
	Workbook wb;
	Sheet sheet;
	
  @Test
  @Parameters("browser")
  public void binvokebrowser(String browser) throws Exception {
	  if(browser.equalsIgnoreCase("firefox")){
			//create firefox instance
				System.setProperty("webdriver.gecko.driver", "C:\\Sid_Folder\\Selenium\\geckodriver-v0.23.0-win64\\geckodriver.exe");
				//System.setProperty("webdriver.firefox.marionette", "C:\\Sid_Folder\\Selenium\\geckodriver-v0.23.0-win32\\geckodriver.exe");
				driver = new FirefoxDriver();
				
			}
			//Check if parameter passed as 'chrome'
			else if(browser.equalsIgnoreCase("chrome")){
				//set path to chromedriver.exe
				System.setProperty("webdriver.chrome.driver","C:\\Sid_Folder\\Selenium\\Chromedriver\\chromedriver_win32\\chromedriver.exe");
				//create chrome instance
				driver = new ChromeDriver();
			}
			
			//Check if parameter passed as 'IE'
			else if(browser.equalsIgnoreCase("ie")){
				
			
		    	System.setProperty("webdriver.ie.driver", "C:\\Sid_Folder\\Selenium\\IEDriverServer_Win32_3.14.0\\IEDriverServer.exe");
		    	driver = new InternetExplorerDriver();
		    	
			}
			
			else{
				//If no browser passed throw exception
				throw new Exception("Browser is not correct");
			}
	    //System.setProperty("webdriver.chrome.driver", "C:\\Sid_Folder\\Selenium\\Chromedriver\\chromedriver_win32\\chromedriver.exe");
		//driver=new ChromeDriver();
		driver.manage().deleteAllCookies();
		driver.manage().window().maximize();
		driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
		driver.manage().timeouts().pageLoadTimeout(30,TimeUnit.SECONDS);


     	//Launch Browser	
    		driver.get("http://ttipsweb03q.tbccorp.com/ntwtips/");
    		Thread.sleep(5000);
    		Reporter.log("TIPS website is launced!");
    			
  }
  
  @Test
  @Parameters("browser")
  public void cWheelorder(String browser) throws Exception, Exception {
	     File src= new File("C:/data.xls");
	     System.out.println("Excel located");	     
	     Workbook wb=Workbook.getWorkbook(src);
	     Sheet sheet=wb.getSheet(0);
	     System.out.println("Workbook loaded");

	/*     String data00=wb.getSheet(0).getCell(0, 0).getContents();
	     System.out.println("Data is " +data00);
	     
	     String data01=wb.getSheet(0).getCell(1, 0).getContents();
	     System.out.println("Data is " +data01);*/
	     
	     int rowCount=sheet.getRows();
	     
	     for(int i=0; i<rowCount; i++)
	     {   		

	    	    String username= sheet.getCell(0, i).getContents();
	     		String password=sheet.getCell(1, i).getContents();
	     		String wheelsize=sheet.getCell(12, i).getContents();
	     		String delivery=sheet.getCell(3,i).getContents();

	     		
	     	//Login
				driver.findElement(By.id("fldAccount")).sendKeys(username);
				driver.findElement(By.id("fldPassword")).sendKeys(password);
				driver.findElement(By.xpath("//button[@type='submit']")).click();
	            System.out.println("username is" +username);
	            System.out.println("Password is" +password);
	            Thread.sleep(3000);
	            driver.findElement(By.xpath("//ul[@id='ntw-top-menu']/li[4]/a/span")).click();
	            Thread.sleep(2000);
	            Reporter.log("Login is successful with username" + username + "entered");
	         //WheelSearch
				driver.findElement(By.id("fldSearchSize")).sendKeys(wheelsize);		
				driver.findElement(By.id("btnSearch")).click();
				Thread.sleep(3000);
				Reporter.log("Wheels Search successful with wheel size" +wheelsize+ "entered");
			//Filters
				//Brand
				driver.findElement(By.xpath("//div[@id='fldFilterBrand_chosen']/ul")).click();
				driver.findElement(By.xpath("//input[@value='All Brands']")).sendKeys("FIRESTONE");
				driver.findElement(By.xpath("//input[@value='All Brands']")).sendKeys(Keys.ENTER);
				Thread.sleep(1000);
				//oos
				driver.findElement(By.cssSelector("#fldFilterOOS_chosen span")).click();
				Thread.sleep(2000);
				driver.findElement(By.cssSelector(".active-result:nth-child(1)")).click();
				//driver.findElement(By.cssSelector("#fldPSFilterSpeedRating_chosen > .chosen-choices")).sendKeys("T");
				Thread.sleep(1000);
				driver.findElement(By.id("btnSearch")).click();
				Thread.sleep(5000);
		     // Take screenshot and store as a file format
	            File TireSrch= ((TakesScreenshot)driver).getScreenshotAs(OutputType.FILE);
	         // now copy the  screenshot to desired location using copyFile //method
	            FileUtils.copyFile(TireSrch, new File("C:/TIPS/WheelSearch" + i+ browser + ".png"));
			//Add to Cart
				driver.findElement(By.xpath("//div[@id='items']/div[5]/div[2]/div[4]/div[3]/div/input")).sendKeys("1");
				driver.findElement(By.xpath("//span/button/i")).click();
				Thread.sleep(5000);
				driver.findElement(By.id("fldCartCount")).click();
				Thread.sleep(3000);
				Reporter.log("Add to Cart successful");
			 // Take screenshot and store as a file format
	            File Cart= ((TakesScreenshot)driver).getScreenshotAs(OutputType.FILE);
	         // now copy the  screenshot to desired location using copyFile //method
	            FileUtils.copyFile(Cart, new File("C:/TIPS/WheelCart" + i+ browser+ ".png"));
				
				//driver.findElement(By.cssSelector("#menuIconCart > i.fas.fa-shopping-cart")).click();
				Thread.sleep(5000);
		    //Checkout				
				driver.findElement(By.cssSelector(".btn-checkout")).click();
				driver.findElement(By.id("fldCheckoutPO")).sendKeys("testpo");
				driver.findElement(By.id("fldCheckoutNotes")).sendKeys("testtext");
				driver.findElement(By.id("fldConName")).sendKeys("tester");
				driver.findElement(By.id("fldConPhone")).sendKeys("1111111111");
				driver.findElement(By.id("fldConEmail")).sendKeys("tester@aol.com");
				driver.findElement(By.id(delivery)).click();
				//driver.findElement(By.xpath("//div[@id='dlgCheckout']/div/div/div[3]/button[2]")).click();
				driver.findElement(By.xpath("//button[contains(.,'Cancel')]")).click();
				Thread.sleep(2000);
				Reporter.log("Checkout successful");
			//Delete from Cart
				driver.findElement(By.xpath("//table[@id='items']/tbody/tr[2]/td/button/i")).click();
				Thread.sleep(2000);
				driver.findElement(By.cssSelector("#dlgSystemResponse .btn-primary")).click();
				Reporter.log("Delete cart successful");
			//Capture Order Number
				//String Ordernumber = driver.findElement(By.xpath("//div[@id='dlgSystemMessage']/div/div/div[2]/p/ul/li")).getText();
				//System.out.println("The order number is:" +Ordernumber);
				//driver.findElement(By.xpath("//div[@id='dlgSystemMessage']/div/div/div[3]/button")).click();
				//Thread.sleep(2000);
			//Back to TireSearch
				Thread.sleep(2000);
				driver.findElement(By.linkText("Continue Shopping")).click();
				Thread.sleep(5000);
				
			//Logout
			//Logout - hover over menu button
	            
	          //if(browser=="ie") {
	          //  Actions builder = new Actions(driver);
	          //  WebElement element = driver.findElement(By.xpath("//ul[@id='ntw-top-menu']/li/a/i"));
	          //  builder.moveToElement(element).perform();	            
	          //  WebElement logout=driver.findElement(By.linkText("Logout"));
	           // WebElement logout=driver.findElement(By.xpath("//ul[@id='ntw-side-menu']/li[14]/a"));
	          //  logout.click();
	          //} else {
				/*driver.findElement(By.xpath("//ul[@id='ntw-top-menu']/li/a/i")).click();
				Thread.sleep(4000);
				driver.findElement(By.xpath("//ul[@id='ntw-side-menu']/li[14]/a")).click();
				Thread.sleep(4000);
				Reporter.log("Logout successful");*/
				//wb.close();            
	          //}            
  }
}
        @Test
  		public void dclosebrowser() {
  		
         //Browser close and Workbook close	
  			//driver.close();
  			driver.quit();
  			
  		}
}





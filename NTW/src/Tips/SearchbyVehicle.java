package Tips;

import org.testng.annotations.Test;
import java.io.File;

import java.util.concurrent.TimeUnit;

import org.testng.Reporter;
import org.testng.annotations.*;
import org.apache.commons.io.FileUtils;
import org.openqa.selenium.By;

import org.openqa.selenium.Keys;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.WebDriver;

import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.ie.InternetExplorerDriver;


import org.openqa.selenium.TakesScreenshot;

import jxl.Sheet;
import jxl.Workbook;



@Test
public class SearchbyVehicle {
	WebDriver driver;
	Workbook wb;
	Sheet sheet;
	
  @Test
  @Parameters("browser")
  public void binvokebrowser(String browser) throws Exception {
	  if(browser.equalsIgnoreCase("firefox")){
			//create firefox instance
				System.setProperty("webdriver.gecko.driver", "C:\\Sid_Folder\\Selenium\\geckodriver-v0.23.0-win64\\geckodriver.exe");
				driver = new FirefoxDriver();
				
			}
			//Check if parameter passed as 'chrome'
			else if(browser.equalsIgnoreCase("chrome")){
				//set path to chromedriver.exe
				System.setProperty("webdriver.chrome.driver","C:\\Sid_Folder\\Selenium\\Chromedriver\\chromedriver_win32\\chromedriver.exe");
				//create chrome instance
				driver = new ChromeDriver();
			}
			
			//Check if parameter passed as 'IE'
			else if(browser.equalsIgnoreCase("ie")){
				
			
		    	System.setProperty("webdriver.ie.driver", "C:\\Sid_Folder\\Selenium\\IEDriverServer_Win32_3.14.0\\IEDriverServer.exe");
		    	driver = new InternetExplorerDriver();
		    	
			}
			
			else{
				//If no browser passed throw exception
				throw new Exception("Browser is not correct");
			}

	    //System.setProperty("webdriver.chrome.driver", "C:\\Sid_Folder\\Selenium\\Chromedriver\\chromedriver_win32\\chromedriver.exe");
		//driver=new ChromeDriver();
		driver.manage().deleteAllCookies();
		driver.manage().window().maximize();
		driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
		driver.manage().timeouts().pageLoadTimeout(30,TimeUnit.SECONDS);


     	//Launch Browser	
    		driver.get("http://ttipsweb03q.tbccorp.com/ntwtips/");
    		Thread.sleep(5000);
    		Reporter.log("TIPS website is launced!");
		     
  }
  
  @Test
  @Parameters("browser")
  public void cSearch(String browser) throws Exception, Exception {
	     File src= new File("C:/data.xls");
	     System.out.println("Excel located");	     
	     Workbook wb=Workbook.getWorkbook(src);
	     Sheet sheet=wb.getSheet(0);
	     System.out.println("Workbook loaded");
	    
	     
	/*     String data00=wb.getSheet(0).getCell(0, 0).getContents();
	     System.out.println("Data is " +data00);
	     
	     String data01=wb.getSheet(0).getCell(1, 0).getContents();
	     System.out.println("Data is " +data01);*/
	     
	     int rowCount=sheet.getRows();
	     
	     for(int i=0; i<rowCount; i++)
	     {   		

	    	    String username= sheet.getCell(0, i).getContents();
	     		String password=sheet.getCell(1, i).getContents();
	     		//String tiresize=sheet.getCell(2, i).getContents();
	     		//String delivery=sheet.getCell(3,i).getContents();
	     		//String local_national=sheet.getCell(4,i).getContents();
	     		String year=sheet.getCell(5,i).getContents();
	     		String make=sheet.getCell(6,i).getContents();
	     		String model=sheet.getCell(7,i).getContents();
	     		String trim=sheet.getCell(8,i).getContents();
	     		String size=sheet.getCell(9,i).getContents();
	     		String brand=sheet.getCell(10,i).getContents();
	     		String line=sheet.getCell(11,i).getContents();


	     	//Login
				driver.findElement(By.id("fldAccount")).sendKeys(username);
				driver.findElement(By.id("fldPassword")).sendKeys(password);
				driver.findElement(By.xpath("//button[@type='submit']")).click();
	            System.out.println("username is" +username);
	            System.out.println("Password is" +password);
	            Thread.sleep(2000);
	            Reporter.log("Login is successful with username" + username + "entered");
	         //TireSearch by vehicle
	            
	         //year
				driver.findElement(By.id("vehic")).click();
				Thread.sleep(1000);
				driver.findElement(By.xpath("//div[@id='fldSearchVehicleYear_chosen']/a/span")).click();
				Thread.sleep(1000);
				//driver.findElement(By.cssSelector("#fldSearchVehicleYear_chosen input")).click();
				//Thread.sleep(2000);
				driver.findElement(By.xpath("//div[@id='fldSearchVehicleYear_chosen']/div/div/input")).sendKeys(year);
				Thread.sleep(1000);
				driver.findElement(By.xpath("//div[@id='fldSearchVehicleYear_chosen']/div/div/input")).sendKeys(Keys.ENTER);
				Thread.sleep(1000);
			//make
				driver.findElement(By.xpath("//div[@id='fldSearchVehicleMake_chosen']/a/span")).click();
				Thread.sleep(1000);
				//driver.findElement(By.cssSelector("#fldSearchVehicleYear_chosen input")).click();
				//Thread.sleep(2000);
				driver.findElement(By.xpath("//div[@id='fldSearchVehicleMake_chosen']/div/div/input")).sendKeys(make);
				Thread.sleep(1000);
				driver.findElement(By.xpath("//div[@id='fldSearchVehicleMake_chosen']/div/div/input")).sendKeys(Keys.ENTER);
				Thread.sleep(1000);
			//model
				driver.findElement(By.xpath("//div[@id='fldSearchVehicleModel_chosen']/a/span")).click();
				Thread.sleep(1000);
				//driver.findElement(By.cssSelector("#fldSearchVehicleYear_chosen input")).click();
				//Thread.sleep(2000);
				driver.findElement(By.xpath("//div[@id='fldSearchVehicleModel_chosen']/div/ul/li[" + model+ "]")).click();
				Thread.sleep(1000);
           //trim
				driver.findElement(By.xpath("//div[@id='fldSearchVehicleTrim_chosen']/a/span")).click();
				Thread.sleep(1000);
				//driver.findElement(By.cssSelector("#fldSearchVehicleYear_chosen input")).click();
				//Thread.sleep(2000);
				driver.findElement(By.xpath("//div[@id='fldSearchVehicleTrim_chosen']/div/ul/li[" + trim+ "]")).click();
				Thread.sleep(1000);
			//size	
				driver.findElement(By.xpath("//div[@id='fldSearchVehicleSize_chosen']/a/span")).click();
				Thread.sleep(1000);
				//driver.findElement(By.cssSelector("#fldSearchVehicleYear_chosen input")).click();
				//Thread.sleep(2000);
				driver.findElement(By.xpath("//div[@id='fldSearchVehicleSize_chosen']/div/ul/li[" + size+ "]")).click();
				Thread.sleep(1000);
			//search	
				driver.findElement(By.id("btnSearch")).click();
				Thread.sleep(3000);
				Reporter.log("Tire Search by vehicle successful with" +year+make+model+trim+ "entered");
			// Take screenshot and store as a file format
	            File vehiclefilter= ((TakesScreenshot)driver).getScreenshotAs(OutputType.FILE);
	        // now copy the  screenshot to desired location using copyFile //method
	            FileUtils.copyFile(vehiclefilter, new File("C:/TIPS/vehiclefilter" + i+browser+ ".png"));	
	            
	            
	       //Tire Search by Brand   
	         //brand   
				driver.findElement(By.linkText("Brand")).click();
				Thread.sleep(1000);
				driver.findElement(By.xpath("//div[@id='fldSearchBrandVendor_chosen']/a/span")).click();
				Thread.sleep(1000);
				//driver.findElement(By.cssSelector("#fldSearchVehicleYear_chosen input")).click();
				//Thread.sleep(2000);
				driver.findElement(By.xpath("//div[@id='fldSearchBrandVendor_chosen']/div/div/input")).sendKeys(brand);
				Thread.sleep(1000);
				driver.findElement(By.xpath("//div[@id='fldSearchBrandVendor_chosen']/div/div/input")).sendKeys(Keys.ENTER);
				Thread.sleep(1000);
			//line	
				driver.findElement(By.xpath("//div[@id='fldSearchBrandLine_chosen']/a/span")).click();
				Thread.sleep(1000);
				//driver.findElement(By.cssSelector("#fldSearchVehicleYear_chosen input")).click();
				//Thread.sleep(2000);
				driver.findElement(By.xpath("//div[@id='fldSearchBrandLine_chosen']/div/ul/li[" + line+ "]")).click();
				Thread.sleep(1000);
			//search	
				driver.findElement(By.id("btnSearch")).click();
				Thread.sleep(5000);				
				Reporter.log("Tire Search by brand successful with" +brand+line+ "entered");
			// Take screenshot and store as a file format
	            File brandfilter= ((TakesScreenshot)driver).getScreenshotAs(OutputType.FILE);
	        // now copy the  screenshot to desired location using copyFile //method
	            FileUtils.copyFile(brandfilter, new File("C:/TIPS/brandfilter" + i+browser+ ".png"));
	            Thread.sleep(5000);	
	            
			//Logout
			//Logout - hover over menu button
		        /*driver.findElement(By.xpath("//ul[@id='ntw-top-menu']/li/a/i")).click();
		        Thread.sleep(4000);
		        driver.findElement(By.xpath("//ul[@id='ntw-side-menu']/li[14]/a")).click();
		        Thread.sleep(4000);       	                   
		        Reporter.log("Logout successful");*/       
			            //Actions builder = new Actions(driver);
			            //WebElement element = driver.findElement(By.xpath("//ul[@id='ntw-top-menu']/li/a/i"));
			            //builder.moveToElement(element).perform();	            
			            //WebElement logout=driver.findElement(By.linkText("Logout"));
			                // WebElement logout=driver.findElement(By.xpath("//ul[@id='ntw-side-menu']/li[14]/a"));
			            //logout.click();
			          
						//wb.close();            
			        
		          
	            
  }
}
        @Test
  		public void dclosebrowser() {
  		
         //Browser close and Workbook close	
  			//driver.close();
  			driver.quit();
  		}
}





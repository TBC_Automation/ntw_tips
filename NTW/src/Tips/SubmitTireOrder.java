package Tips;

import org.testng.annotations.Test;
import java.io.File;

import java.util.concurrent.TimeUnit;

import org.testng.Reporter;
import org.testng.annotations.*;
import org.apache.commons.io.FileUtils;
import org.openqa.selenium.By;

import org.openqa.selenium.OutputType;
import org.openqa.selenium.WebDriver;

import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.ie.InternetExplorerDriver;

import org.openqa.selenium.TakesScreenshot;

import jxl.Sheet;
import jxl.Workbook;



@Test
public class SubmitTireOrder {
	WebDriver driver;
	Workbook wb;
	Sheet sheet;
	
  @Test
  @Parameters("browser")
  public void binvokebrowser(String browser) throws Exception {
	  if(browser.equalsIgnoreCase("firefox")){
			//create firefox instance
				System.setProperty("webdriver.gecko.driver", "C:\\Sid_Folder\\Selenium\\geckodriver-v0.23.0-win64\\geckodriver.exe");
				//System.setProperty("webdriver.firefox.marionette", "C:\\Sid_Folder\\Selenium\\geckodriver-v0.23.0-win32\\geckodriver.exe");
				driver = new FirefoxDriver();
				
			}
			//Check if parameter passed as 'chrome'
			else if(browser.equalsIgnoreCase("chrome")){
				//set path to chromedriver.exe
				System.setProperty("webdriver.chrome.driver","C:\\Sid_Folder\\Selenium\\Chromedriver\\chromedriver_win32\\chromedriver.exe");
				//create chrome instance
				driver = new ChromeDriver();
			}
			
			//Check if parameter passed as 'IE'
			else if(browser.equalsIgnoreCase("ie")){
				
			
		    	System.setProperty("webdriver.ie.driver", "C:\\Sid_Folder\\Selenium\\IEDriverServer_Win32_3.14.0\\IEDriverServer.exe");
		    	driver = new InternetExplorerDriver();
		    	
			}
			
			else{
				//If no browser passed throw exception
				throw new Exception("Browser is not correct");
			}
	    //System.setProperty("webdriver.chrome.driver", "C:\\Sid_Folder\\Selenium\\Chromedriver\\chromedriver_win32\\chromedriver.exe");
		//driver=new ChromeDriver();
		driver.manage().deleteAllCookies();
		driver.manage().window().maximize();
		driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
		driver.manage().timeouts().pageLoadTimeout(30,TimeUnit.SECONDS);


     	//Launch Browser	
    		driver.get("http://ttipsweb03q.tbccorp.com/ntwtips/");
    		Thread.sleep(5000);
    		Reporter.log("TIPS website is launced!");
		
  }
  
  @Test
  @Parameters("browser")
  public void cordersubmit(String browser) throws Exception, Exception {
	     File src= new File("C:/data.xls");
	     System.out.println("Excel located");	     
	     Workbook wb=Workbook.getWorkbook(src);
	     Sheet sheet=wb.getSheet(0);
	     System.out.println("Workbook loaded");
	    
	/*     String data00=wb.getSheet(0).getCell(0, 0).getContents();
	     System.out.println("Data is " +data00);
	     
	     String data01=wb.getSheet(0).getCell(1, 0).getContents();
	     System.out.println("Data is " +data01);*/
	     
	     int rowCount=sheet.getRows();
	     
	     for(int i=0; i<rowCount; i++)
	     {   		

	    	    String username= sheet.getCell(0, i).getContents();
	     		String password=sheet.getCell(1, i).getContents();
	     		String tiresize=sheet.getCell(2, i).getContents();
	     		String delivery=sheet.getCell(3,i).getContents();


	     		
	     	//Login
				driver.findElement(By.id("fldAccount")).sendKeys(username);
				driver.findElement(By.id("fldPassword")).sendKeys(password);
				driver.findElement(By.xpath("//button[@type='submit']")).click();
	            System.out.println("username is" +username);
	            System.out.println("Password is" +password);
	            Thread.sleep(5000);
	            Reporter.log("Login is successful with username" + username + "entered");
	         // Take screenshot and store as a file format
	            File login= ((TakesScreenshot)driver).getScreenshotAs(OutputType.FILE);
	         // now copy the  screenshot to desired location using copyFile //method
	            FileUtils.copyFile(login, new File("C:/TIPS/login" +browser+ ".png"));
	         //TireSearch
				driver.findElement(By.id("fldSearchSize")).sendKeys(tiresize);
				driver.findElement(By.id("btnSearch")).click();
				Thread.sleep(5000);
				Reporter.log("Tire Search successful with wheel size" +tiresize+ "entered");
		     // Take screenshot and store as a file format
	            File TireSrch= ((TakesScreenshot)driver).getScreenshotAs(OutputType.FILE);
	         // now copy the  screenshot to desired location using copyFile //method
	            FileUtils.copyFile(TireSrch, new File("C:/TIPS/TSearch" + i+ browser + ".png"));
			//Add to Cart
				driver.findElement(By.xpath("//input[@value='']")).sendKeys("1");
				driver.findElement(By.xpath("(//button[@type='button'])[12]")).click();
				Thread.sleep(5000);
				driver.findElement(By.id("fldCartCount")).click();
				Thread.sleep(3000);
				Reporter.log("Add to Cart successful");
			 // Take screenshot and store as a file format
	            File Cart= ((TakesScreenshot)driver).getScreenshotAs(OutputType.FILE);
	         // now copy the  screenshot to desired location using copyFile //method
	            FileUtils.copyFile(Cart, new File("C:/TIPS/Cart" + i+ browser+ ".png"));
				
				//driver.findElement(By.cssSelector("#menuIconCart > i.fas.fa-shopping-cart")).click();
				Thread.sleep(5000);
		    //Checkout				
				driver.findElement(By.xpath("//div[@id='content']/form/span/button[3]")).click();
				driver.findElement(By.id("fldCheckoutPO")).sendKeys("testpo");
				driver.findElement(By.id("fldCheckoutNotes")).sendKeys("testtext");
				driver.findElement(By.id("fldConName")).sendKeys("tester");
				driver.findElement(By.id("fldConPhone")).sendKeys("1111111111");
				driver.findElement(By.id("fldConEmail")).sendKeys("tester@aol.com");
				driver.findElement(By.id(delivery)).click();
				//driver.findElement(By.xpath("//div[@id='dlgCheckout']/div/div/div[3]/button[2]")).click();
				driver.findElement(By.xpath("//button[contains(.,'Cancel')]")).click();
				Thread.sleep(2000);
				Reporter.log("Checkout successful");
			//Delete from Cart
				driver.findElement(By.xpath("//table[@id='items']/tbody/tr[2]/td/button/i")).click();
				Thread.sleep(2000);
				driver.findElement(By.cssSelector("#dlgSystemResponse .btn-primary")).click();
				Reporter.log("Delete cart successful");
				Thread.sleep(2000);
				driver.quit();
			//Capture Order Number
				//String Ordernumber = driver.findElement(By.xpath("//div[@id='dlgSystemMessage']/div/div/div[2]/p/ul/li")).getText();
				//System.out.println("The order number is:" +Ordernumber);
				//driver.findElement(By.xpath("//div[@id='dlgSystemMessage']/div/div/div[3]/button")).click();
				//Thread.sleep(2000);

		
      }
     }
  
			  @Test
				public void dclosebrowser() {
				
			   //Browser close and Workbook close	
					//driver.close();
					driver.quit();
  }
}







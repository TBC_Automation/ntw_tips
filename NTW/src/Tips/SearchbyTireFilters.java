package Tips;

import org.testng.annotations.Test;
import java.io.File;

import java.util.concurrent.TimeUnit;

import org.testng.Reporter;
import org.testng.annotations.*;
import org.apache.commons.io.FileUtils;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.Keys;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.WebDriver;

import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.ie.InternetExplorerDriver;

import org.openqa.selenium.TakesScreenshot;

import jxl.Sheet;
import jxl.Workbook;



@Test
public class SearchbyTireFilters {
	WebDriver driver;
	Workbook wb;
	Sheet sheet;
	
  @Test
  @Parameters("browser")
  public void binvokebrowser(String browser) throws Exception {
	  if(browser.equalsIgnoreCase("firefox")){
			//create firefox instance
				System.setProperty("webdriver.gecko.driver", "C:\\Sid_Folder\\Selenium\\geckodriver-v0.23.0-win64\\geckodriver.exe");
				//System.setProperty("webdriver.firefox.marionette", "C:\\Sid_Folder\\Selenium\\geckodriver-v0.23.0-win32\\geckodriver.exe");
				driver = new FirefoxDriver();
				
			}
			//Check if parameter passed as 'chrome'
			else if(browser.equalsIgnoreCase("chrome")){
				//set path to chromedriver.exe
				System.setProperty("webdriver.chrome.driver","C:\\Sid_Folder\\Selenium\\Chromedriver\\chromedriver_win32\\chromedriver.exe");
				//create chrome instance
				driver = new ChromeDriver();
			}
			
			//Check if parameter passed as 'IE'
			else if(browser.equalsIgnoreCase("ie")){
				
			
		    	System.setProperty("webdriver.ie.driver", "C:\\Sid_Folder\\Selenium\\IEDriverServer_Win32_3.14.0\\IEDriverServer.exe");
		    	driver = new InternetExplorerDriver();
		    	
			}
			
			else{
				//If no browser passed throw exception
				throw new Exception("Browser is not correct");
			}
	    //System.setProperty("webdriver.chrome.driver", "C:\\Sid_Folder\\Selenium\\Chromedriver\\chromedriver_win32\\chromedriver.exe");
		//driver=new ChromeDriver();
		driver.manage().deleteAllCookies();
		driver.manage().window().maximize();
		driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
		driver.manage().timeouts().pageLoadTimeout(30,TimeUnit.SECONDS);


     	//Launch Browser	
    		driver.get("http://ttipsweb03q.tbccorp.com/ntwtips/");
    		Thread.sleep(5000);
    		Reporter.log("TIPS website is launced!");
		
  }
  
  @Test
  @Parameters("browser")
  public void cFiltersearch(String browser) throws Exception, Exception {
	     File src= new File("C:/data.xls");
	     System.out.println("Excel located");	     
	     Workbook wb=Workbook.getWorkbook(src);
	     Sheet sheet=wb.getSheet(0);
	     System.out.println("Workbook loaded");
	     JavascriptExecutor js = (JavascriptExecutor) driver;
	/*     String data00=wb.getSheet(0).getCell(0, 0).getContents();
	     System.out.println("Data is " +data00);
	     
	     String data01=wb.getSheet(0).getCell(1, 0).getContents();
	     System.out.println("Data is " +data01);*/
	     
	     int rowCount=sheet.getRows();
	     
	     for(int i=0; i<rowCount; i++)
	     {   		

	    	    String username= sheet.getCell(0, i).getContents();
	     		String password=sheet.getCell(1, i).getContents();
	     		String tiresize=sheet.getCell(2, i).getContents();
	     		String local_national=sheet.getCell(4,i).getContents();

	     		
	     	//Login
				driver.findElement(By.id("fldAccount")).sendKeys(username);
				driver.findElement(By.id("fldPassword")).sendKeys(password);
				driver.findElement(By.xpath("//button[@type='submit']")).click();
	            System.out.println("username is" +username);
	            System.out.println("Password is" +password);
	            Thread.sleep(5000);
	            Reporter.log("Login is successful with username" + username + "entered");
	        
	         //TireSearch
				driver.findElement(By.id("fldSearchSize")).sendKeys(tiresize);
				driver.findElement(By.id("btnSearch")).click();
				Thread.sleep(5000);
				Reporter.log("Tire Search successful with tire size" +tiresize+ "entered");

			//Search Filters
				//select Mast
				driver.findElement(By.id("chkMAST")).click();
				Thread.sleep(2000);
				//select OE
				driver.findElement(By.cssSelector("#fldPSFilterOEFitment_chosen > .chosen-choices")).click();
				Thread.sleep(1000);
				driver.findElement(By.cssSelector("#fldPSFilterOEFitment_chosen .default")).sendKeys("NO");
				Thread.sleep(1000);
				driver.findElement(By.cssSelector("#fldPSFilterOEFitment_chosen .default")).sendKeys(Keys.ENTER);
				Thread.sleep(1000);
				//Select warranty
				driver.findElement(By.cssSelector("#fldPSFilterWarrantyMiles_chosen > .chosen-choices")).click();
				Thread.sleep(1000);
				driver.findElement(By.cssSelector("#fldPSFilterWarrantyMiles_chosen .default")).sendKeys("70,000");
				Thread.sleep(1000);
				driver.findElement(By.cssSelector("#fldPSFilterWarrantyMiles_chosen .default")).sendKeys(Keys.ENTER);
				Thread.sleep(1000);
				//select speed
				driver.findElement(By.cssSelector("#fldPSFilterSpeedRating_chosen > .chosen-choices")).click();
				Thread.sleep(1000);
				driver.findElement(By.cssSelector("#fldPSFilterSpeedRating_chosen .default")).sendKeys("T");
				Thread.sleep(1000);
				driver.findElement(By.cssSelector("#fldPSFilterSpeedRating_chosen .default")).sendKeys(Keys.ENTER);
				Thread.sleep(1000);
				//select load
				driver.findElement(By.cssSelector("#fldPSFilterLoadIndex_chosen > .chosen-choices")).click();
				Thread.sleep(1000);
				driver.findElement(By.cssSelector("#fldPSFilterLoadIndex_chosen .default")).sendKeys("98");
				Thread.sleep(1000);
				driver.findElement(By.cssSelector("#fldPSFilterLoadIndex_chosen .default")).sendKeys(Keys.ENTER);
				//select ply
				driver.findElement(By.cssSelector("#fldPSFilterPly_chosen > .chosen-choices")).click();
				Thread.sleep(1000);				
				driver.findElement(By.cssSelector("#fldPSFilterPly_chosen .default")).sendKeys("SL");
				Thread.sleep(1000);
				driver.findElement(By.cssSelector("#fldPSFilterPly_chosen .default")).sendKeys(Keys.ENTER);
				Thread.sleep(1000);
				Reporter.log("Filter search for tires successful");
			 // Take screenshot and store as a file format
	            File Filtersize= ((TakesScreenshot)driver).getScreenshotAs(OutputType.FILE);
	         // now copy the  screenshot to desired location using copyFile //method
	            FileUtils.copyFile(Filtersize, new File("C:/TIPS/Filtersize" + i+browser+ ".png"));	
			//Qty search
	            js.executeScript("window.scrollBy(0,300)");
				driver.findElement(By.id("filter-result-btn")).click();
				Thread.sleep(1000);
				driver.findElement(By.id(local_national)).click();
				Thread.sleep(1000);
				driver.findElement(By.id("filter-available-qty")).sendKeys("10");
				Thread.sleep(1000);				
				driver.findElement(By.id("apply-filter-btn")).click();
				Thread.sleep(3000);
				Reporter.log("Qty search successful");
			// Take screenshot and store as a file format
	            File qtyfilter= ((TakesScreenshot)driver).getScreenshotAs(OutputType.FILE);
	        // now copy the  screenshot to desired location using copyFile //method
	            FileUtils.copyFile(qtyfilter, new File("C:/TIPS/qtyfilter" + i+ browser+ ".png"));	
			//Logout
			//Logout - hover over menu button
	            
	          //if(browser=="ie") {
	          //  Actions builder = new Actions(driver);
	          //  WebElement element = driver.findElement(By.xpath("//ul[@id='ntw-top-menu']/li/a/i"));
	          //  builder.moveToElement(element).perform();	            
	          //  WebElement logout=driver.findElement(By.linkText("Logout"));
	           // WebElement logout=driver.findElement(By.xpath("//ul[@id='ntw-side-menu']/li[14]/a"));
	          //  logout.click();
	          //} else {
				/*driver.findElement(By.xpath("//ul[@id='ntw-top-menu']/li/a/i")).click();
				Thread.sleep(4000);
				driver.findElement(By.xpath("//ul[@id='ntw-side-menu']/li[14]/a")).click();
				Thread.sleep(4000);
				Reporter.log("Logout successful");*/
				//wb.close();            
	          //}            
  }
}
        @Test
  		public void dclosebrowser() {
  		
         //Browser close and Workbook close	
  			//driver.close();
  			driver.quit();
  			
  		}
}




